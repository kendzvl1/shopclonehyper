<title><?=$site['site_tenweb'];?></title>
<div class="mobile-menu-overlay"></div>

<div class="main-container">
    <div class="pd-ltr-20">
        <div class="card-box pd-20 height-100-p mb-30">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <p class="font-18 max-width-600"><?=$site['site_thong_bao'];?></p>
                </div>
            </div>
        </div>
        <!-- multiple select row Datatable start -->
        <div class="card-box mb-30">
            <div class="pd-20">
                <h4 class="text-blue h4">DANH SÁCH TÀI KHOẢN</h4>
            </div>
            <div class="pd-20 card-box height-100-p">
                
               @include('Action.Buy')
            </div>
        </div>
        <?php if($site['gdganday'] == 'ON'){?>
        <div class="card-box mb-30">
            <div class="pd-20">
                <h4 class="text-blue h4">50 GIAO DỊCH GẦN ĐÂY</h4>
               
            </div>
            <div class="pd-20 card-box height-100-p">
                <div class="table-responsive">
                    <table class="table hover multiple-select-row nowrap">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Khách Hàng</th>
                                <th>Giao Dịch</th>
                                <th>Thời Gian</th>
                            </tr>
                        </thead>
                        <tbody>
             
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- multiple select row Datatable End -->
         <?php }?>